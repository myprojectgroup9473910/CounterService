#!flask/bin/python
from flask import Flask, request

app = Flask(__name__)
counter = 0

@app.route('/', methods=["POST", "GET"])
def index():
    global counter
    if request.method == "POST" or request.method == "GET":
        counter += 1
        return f'Our counter is: {counter}'
    else:
        return "Method not allowed", 405

if __name__ == '__main__':
    app.run(debug=True, port=443, host='0.0.0.0', ssl_context='adhoc')
