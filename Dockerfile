# Use the official Python image as a parent image
FROM python:3.8-slim

# Set the working directory in the container
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY . /app

# Install Flask
RUN pip install --no-cache-dir flask

# Expose port 443 for HTTPS
EXPOSE 443

# Specify the command to run on container start
CMD ["python", "counter-service.py"]
